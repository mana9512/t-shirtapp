import {
  ADD_ITEM,
  ITEM_ADDED,
  REMOVE_ITEM,

} from "../actions/types";

const initialState = {
  products: [
    {
      _id: "1",
      title: "Vero Moda",
      src: "images/Top/Women Black Printed A-Line Top.png",
      description: "Women Black Printed A-Line Top",
      category: "top",
      price: 23,
      count: 0,
    },
    {
      _id: "2",
      title: "Vero Moda",
      src: "images/Top/Women Black Solid Peplum Top.png",
      description: "Women Black Solid Peplum Top",
      category: "top",
      price: 19,
      count: 0,
    },
    {
      _id: "3",
      title: "Vero Moda",
      src: "images/Top/Women Black Solid Styled Back Crop Top.png",
      description: "Women Black Solid Styled Top",
      category: "top",
      price: 50,
      count: 0,
    },
    {
      _id: "4",
      title: "Vero Moda",
      src: "images/T Shirt/Women Dark Pink Printed Round Neck T-shirt.png",
      description: "Women Dark Pink Printed  Neck T-shirt",
      category: "tshirt",
      price: 15,
      count: 0,
    },
    {
      _id: "5",
      title: "Vero Moda",
      src:
        "images/T Shirt/Women Pink Solid Kora Natural Dyed Round Neck T-shirt.png",
      description: "Women Pink Solid Kora Natural  T-shirt",
      category: "tshirt",
      price: 10,
      count: 0,
    },
    {
      _id: "6",
      title: "Vero Moda",
      src: "images/T Shirt/Women White Solid Round Neck T-shirt.png",
      description: "Women White Solid Round Neck T-shirt",
      category: "tshirt",
      price: 17,
      count: 0,
    },
    {
      _id: "7",
      title: "Vero Moda",
      src: "images/Top/Women Navy Blue Printed Top.png",
      description: "Women Navy Blue Printed Top",
      category: "top",
      price: 23,

      count: 0,
    },
    {
      _id: "8",
      title: "Vero Moda",
      src: "images/Top/Women Black Solid Victorian Crop Fitted Top.png",
      description: "Women Black Solid Fitted Top",
      category: "top",
      price: 19,

      count: 0,
    },
    {
      _id: "9",
      title: "Vero Moda",
      src: "images/T Shirt/Women Lavender Printed Round Neck T-shirt.png",
      description: "Women Lavender Printed T-shirt",
      category: "tshirt",
      price: 50,
      count: 0,
    },
    {
      _id: "10",
      title: "Vero Moda",
      src:
        "images/Top/Women Mustard Yellow Solid Twisted Cropped Fitted Top.png",
      description: "Women Mustard Yellow Fitted Top",
      category: "top",
      price: 15,
      count: 0,
    },
    {
      _id: "11",
      title: "Vero Moda",
      src: "images/T Shirt/Women Pink Printed Round Neck T-shirt.png",
      description: "Women Pink Printed Round Neck T-shirt",
      category: "tshirt",
      price: 10,
      count: 0,
    },
    {
      _id: "12",
      title: "Vero Moda",
      src: "images/Top/Women White Solid Blouson Top.png",
      description: "Women White Solid Blouson Top",
      category: "top",
      price: 17,
      count: 0,
    },
  ],
  cart: [],
  total: 0
};

export default function (state = initialState, action) {
  const { type, payload } = action;
  var products = [...state.products];

  if (type === ITEM_ADDED) {

    const check = state.cart.every((item) => {
      return item._id !== payload;
    });
    const addeditem = products.filter((product) => {
      return product._id === payload;
    });

    if (check) {
      addeditem.count = 1
      localStorage.setItem("dataCart", JSON.stringify([...state.cart, addeditem]));
      localStorage.setItem("dataTotal", JSON.stringify(state.total + addeditem.price));
      return {
        ...state,
        cart: [...state.cart, addeditem],
        total: state.total + addeditem.price
      }

    }
    else {
      localStorage.setItem("dataTotal", JSON.stringify(state.total + addeditem.price));
      addeditem.count += 1;
      var dataCart = JSON.parse(localStorage.getItem("dataCart"));
        const updatedcart=dataCart.filter(cartitem => {
          return cartitem[0]._id!==payload
          
        });
        localStorage.setItem("dataCart", JSON.stringify([...updatedcart,addeditem]));
      return {
        ...state,
        total: state.total + addeditem.price
      }

    }

  }
  else if (type === ADD_ITEM) {
        
        const addeditem = products.filter((product) => {
          return product._id === payload;
        });
        addeditem.count = 1
        addeditem.count+=1
        console.log(addeditem);
        var dataCart = JSON.parse(localStorage.getItem("dataCart"));
        const updatedcart=dataCart.filter(cartitem => {
          return cartitem[0]._id!==payload
          
        });
        localStorage.setItem("dataCart", JSON.stringify([...updatedcart,addeditem]));
        localStorage.setItem("dataTotal", JSON.stringify(state.total + addeditem.price));
        return {
          ...state,
          total: state.total + addeditem.price
        }

  }
  else if (type === REMOVE_ITEM) {

        const addeditem = products.filter((product) => {
          return product._id === payload;
        });
        if(addeditem.count ===1){
        let newcart=state.cart.filter(cartitem => cartitem._id !== payload)
        localStorage.setItem("dataCart", JSON.stringify(newcart));
        localStorage.setItem("dataTotal", JSON.stringify(state.total + addeditem.price));
        return {
          ...state,
          total: state.total - (addeditem.price* addeditem.count)
        }
        }
        else
        {
              addeditem.count -= 1
              localStorage.setItem("dataTotal", JSON.stringify(state.total - addeditem.price*addeditem.count));
              return {
                ...state,
                total: state.total - addeditem.price
              }
        }

        
  }
  else {
    return state;
  }
}