import React, { Fragment } from "react";
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import "./components/layout/Navbar";
import  {Navbar}from "./components/layout/Navbar";
import  Landing  from "./components/layout/Landing";
import  Cart  from "./components/Cart";
import  ProductDetail  from "./components/ProductDetail";
import  {ProductProvider } from "./components/ProductContext";
import store from "./store";
import { Provider } from "react-redux";

const App = () => {
  return (
    <Provider store={store}>
    <ProductProvider>
      <Router>
        <Fragment>
          <Navbar />
          <Switch>
            <Route exact path="/product/:id" component={ProductDetail} />
            <Route exact path="/cart" component={Cart} />
            <Route exact path="/" component={Landing} />
          </Switch>
        </Fragment>
      </Router>
    </ProductProvider>
    </Provider>
  );
};

export default App;
