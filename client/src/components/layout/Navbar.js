import React, { Fragment, useContext } from 'react'
import { ShoppingCartOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom'
import 'antd/dist/antd.css';
import { Layout, Menu } from 'antd';
import { Badge } from 'antd';
import { ProductContext } from '../ProductContext'

import PropTypes from "prop-types";
import { connect } from "react-redux";
const { SubMenu } = Menu;
const { Header } = Layout;



export const Navbar = () => {
  //const [dup, cart, setCart, addCart] = useContext(ProductContext);

  var dataCart = JSON.parse(localStorage.getItem("dataCart"));
  if (dataCart == null) {
    dataCart = []
  }



  return (
    <Fragment>
      <Header className="header">
        <Link to="/"><div className="logo" > <img alt="example" src={"images/logo.png"} /></div></Link>
        <Menu theme="light" mode="horizontal" defaultSelectedKeys={['2']}>
          <Menu.Item key="1" className="floatRight">Sign Up</Menu.Item>
          <Menu.Item key="2" className="floatRight">Sign In</Menu.Item>
          <Menu.Item key="3" className="floatRight"> <Link to="/cart">Cart <Badge count={dataCart.length} offset={[-1, 1]}><ShoppingCartOutlined style={{ fontSize: '20px' }} /> </Badge></Link></Menu.Item>
        </Menu>
      </Header>
    </Fragment>
  )
}

