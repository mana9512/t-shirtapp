import React, { Fragment, useContext, useState, } from 'react'
import 'antd/dist/antd.css';
import { Layout, Menu, Breadcrumb } from 'antd';
import { UserOutlined, ShoppingCartOutlined } from '@ant-design/icons';
import { Card, Button } from 'antd';
import { Row, Col } from 'antd';
import { Link } from 'react-router-dom'
//import { ProductContext } from '../ProductContext'

import PropTypes from "prop-types";
import { connect } from "react-redux";
import { addToCart } from "../../actions/cart"

const { SubMenu } = Menu;
const { Content, Footer, Sider } = Layout;


const { Meta } = Card;


const Landing = ({cart,products,addToCart}) => {

  //const [dup, cart, setCart, addCart] = useContext(ProductContext);
  var [dup, setProducts] = useState([...products]);
  var top = [...products];
  top = top.filter(product => { return product.category === "top" });
  var shirt = [...products];
  shirt = shirt.filter(product => { return product.category === "tshirt" })
  var all = [...products];
  
  const addCart= (id) =>{
    addToCart(id)
  }


  const update = (val) => {

    if (val === 1) {

      setProducts(all);
    }
    else if (val === 2) {

      setProducts(top);
    }
    else {
      setProducts(shirt);
    }

  }

  return (
    <Fragment>
      <Layout>

        <Content style={{ padding: '0 50px' }}>

          <Layout className="site-layout-background" style={{ padding: '24px 0' }}>
            <Sider className="site-layout-background" width={200}>
              <Menu
                mode="inline"
                defaultSelectedKeys={['1']}
                defaultOpenKeys={['sub1']}
                style={{ height: '100%' }}
              >
                <SubMenu key="sub1" icon={<UserOutlined />} title="Categories">
                  <Menu.Item key="1" onClick={(e) => update(1)}>All</Menu.Item>
                  <Menu.Item key="2" onClick={(e) => update(2)}>Tops</Menu.Item>
                  <Menu.Item key="3" onClick={(e) => update(3)}>T-shirts</Menu.Item>

                </SubMenu>

              </Menu>
            </Sider>
            <Content style={{ padding: '0 24px', minHeight: 280 }} className="bgWhite">
              <br />
              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
                {
                  dup.map(product => (
                    <Col key={product._id} className="gutter-row" span={6}>
                      
                        <Card
                          hoverable={true}
                          style={{ width: 300 }}
                          cover={
                            <img
                              alt="example"
                              src={product.src}
                            />
                          }

                        >
                          <Link to={`/product/${product._id}`}><Meta

                            title="Vero Moda"
                            description={product.description}
                          /></Link>
                          <br />
                          <span><b>${product.price}</b></span>&emsp;&emsp;
                    <Button onClick={() => addCart(product._id)} width="300px" className="cartbtn" height={200} type="primary" icon={<ShoppingCartOutlined />}>Add to Cart</Button>
                        </Card>
                     
                      <br />
                    </Col>
                  )
                  )}

              </Row>
              <br />



            </Content>
          </Layout>
        </Content>
        <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
      </Layout>,
    </Fragment>
  )
}
Landing.propTypes = {

  addToCart: PropTypes.func.isRequired,
  products: PropTypes.array.isRequired,
  cart: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  products: state.cart.products,
  cart:state.cart.cart,
});
const mapDispatchToProps = (dispatch) =>{
  return{
    addToCart:(id)=>{dispatch(addToCart(id))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps )(Landing)