import React, { Fragment, useContext } from "react";
import { Row, Col } from "antd";
import ReactDOM from "react-dom";
import { ShoppingCartOutlined } from "@ant-design/icons";
import "antd/dist/antd.css";
import { Image } from "antd";
import { Button, Tooltip } from "antd";
import { ProductContext } from "../components/ProductContext";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { addToCart } from "../actions/cart"

 const ProductDetail = ({ match ,products, addToCart}) => {

  //const [dup, cart, addCart] = useContext(ProductContext);
  
  var dup = [...products];
  const data = dup.filter((product) => {
    return product._id === match.params.id;
  });
  const addCart= (id) =>{
    addToCart(id)
  }

  return (
    <Fragment>
      <br />
      <br />
      <Row>
        <br />
        <Col span={12}>
          <Image width={400} className="pimg" src={data[0].src} />
        </Col>
        <Col span={12}>
          <div className="ptitle row">{data[0].title}</div>
          <hr />
          <div className="d row">{data[0].description}</div>
          <span className="d">
            <b>${data[0].price}</b>
          </span>
          <br />
          <Button
            width="300px"
            className="cartbtn"
            height={200}
            type="primary"
            color="blue"
            icon={<ShoppingCartOutlined />}
            onClick={() => addCart(data[0]._id)}
          >
            Add to Cart
          </Button>
          <br />
          <br />
          <div className="dd ">
            100% Original Products
            <br />
            Free Delivery on order above Rs. 799
            <br />
            Pay on delivery might be available
            <br />
            Easy 30 days returns and exchanges
            <br />
            Try & Buy might be available
            <br />
          </div>
        </Col>
      </Row>
    </Fragment>
  );
};

ProductDetail.propTypes = {

  addToCart: PropTypes.func.isRequired,
  products: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  products: state.cart.products,
});
const mapDispatchToProps = (dispatch) =>{
  return{
    addToCart:(id)=>{dispatch(addToCart(id))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps )(ProductDetail)