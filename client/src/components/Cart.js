import React, { Fragment, useContext,useState } from "react";
import { Row, Col } from "antd";
import { Image } from "antd";
import { Button } from "antd";
import { PlusOutlined, MinusOutlined ,DeleteOutlined} from "@ant-design/icons";
import { ProductContext } from "../components/ProductContext";

import PropTypes from "prop-types";
import { connect } from "react-redux";
import { addItem,removeFromCart,removeItem } from "../actions/cart"

 const Cart = ({cart,addItem,removeFromCart,removeItem}) => {
  //const [dup, cart, setCart, addCart, addQuantity, removeQuantity, total,totalAmount, removeFromCart] = useContext(ProductContext);
  const dataCart = JSON.parse(localStorage.getItem("dataCart"));
// dataCart.map(d=> console.log(d[0]))
  console.log(dataCart[0]);
  var dataTotal = JSON.parse(localStorage.getItem('dataTotal'));
  if(dataTotal==null){
    dataTotal=0
  }
  
  const addItems=(id) =>{
      addItem(id);
  }
  const removeItems=(id) =>{
      removeItem(id)
   }
  const removeFromCarts=(id) =>{
      removeFromCart(id)
  }
  
  return (
    <Fragment>
     
      <div>
        {dataCart.length>0 ?dataCart.map((cartitem) => {
          return (
            <div className="boxed" key={cartitem[0]._id}>
              <Row>
              
                <Col span={3} offset={1}>
                  <Image
                    style={{ paddingTop: "35px" }}
                    width={240}
                    height={300}
                    src={cartitem[0].src}
                    preview={false}
                  />
                </Col>
                <Col span={6} offset={4}>
                  <div style={{ paddingTop: "35px" }}>
                    <div className="cartinfo" >
                      <span>
                        <b>{cartitem[0].title}</b>
                      </span>
                     
                    </div>
                    
                    <br />
                    <div className="cartinfo">{cartitem[0].description}</div>
                    <br />
            
                    <div className="cartinfo">
                      <span>
                        <b>${cartitem[0].price}</b>
                      </span>
                    </div>

                    <br />
                    <Button icon={<PlusOutlined />} onClick={()=>addItems(cartitem[0]._id)}></Button>&nbsp;&nbsp;
                    <span className="cartinfo">{cartitem[0].count}</span>&nbsp;&nbsp;
                    <Button icon={<MinusOutlined />} onClick={()=>removeItems(cartitem[0]._id)}></Button>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                    <Button className="floatRight" icon={<DeleteOutlined />} onClick={()=>removeFromCarts(cartitem[0]._id)}></Button>
                  </div>
                </Col>
                
              </Row>
              <br />
            </div>
          );
        })
      :"NO ITEMS IN CART"}
      </div>
      <Row>
        <Col offset={19}>
          <span style={{ fontSize: "25px" }}>
            <b>Total: ${dataTotal}</b>
          </span>
        </Col>
      </Row>
    </Fragment>
  );
};
Cart.propTypes = {

  addItem:PropTypes.func.isRequired,
  removeItem:PropTypes.func.isRequired,
  removeFromCart:PropTypes.func.isRequired,
  cart: PropTypes.array.isRequired,
  total:PropTypes.number.isRequired
};

const mapStateToProps = (state) => ({
  products: state.cart.products,
  cart:state.cart,
  total:state.cart.total
});
const mapDispatchToProps = (dispatch) =>{
  return{
    addItem:(id)=>{dispatch(addItem(id))},
    removeItem: (id)=>{dispatch(removeItem(id))},
    removeFromCart:(id)=>{dispatch(removeFromCart(id))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps )(Cart)