import React, { useState, createContext, useEffect, useRef } from "react";

export const ProductContext = React.createContext();

export const ProductProvider = (props) => {
  const [dup, setDup] = useState([
    {
      _id: "1",
      title: "Vero Moda",
      src: "images/Top/Women Black Printed A-Line Top.png",
      description: "Women Black Printed A-Line Top",
      category: "top",
      price: 23,
      count: 1,
    },
    {
      _id: "2",
      title: "Vero Moda",
      src: "images/Top/Women Black Solid Peplum Top.png",
      description: "Women Black Solid Peplum Top",
      category: "top",
      price: 19,
      count: 1,
    },
    {
      _id: "3",
      title: "Vero Moda",
      src: "images/Top/Women Black Solid Styled Back Crop Top.png",
      description: "Women Black Solid Styled Top",
      category: "top",
      price: 50,
      count: 1,
    },
    {
      _id: "4",
      title: "Vero Moda",
      src: "images/T Shirt/Women Dark Pink Printed Round Neck T-shirt.png",
      description: "Women Dark Pink Printed  Neck T-shirt",
      category: "tshirt",
      price: 15,
      count: 1,
    },
    {
      _id: "5",
      title: "Vero Moda",
      src:
        "images/T Shirt/Women Pink Solid Kora Natural Dyed Round Neck T-shirt.png",
      description: "Women Pink Solid Kora Natural  T-shirt",
      category: "tshirt",
      price: 10,
      count: 1,
    },
    {
      _id: "6",
      title: "Vero Moda",
      src: "images/T Shirt/Women White Solid Round Neck T-shirt.png",
      description: "Women White Solid Round Neck T-shirt",
      category: "tshirt",
      price: 17,
      count: 1,
    },
    {
      _id: "7",
      title: "Vero Moda",
      src: "images/Top/Women Navy Blue Printed Top.png",
      description: "Women Navy Blue Printed Top",
      category: "top",
      price: 23,

      count: 1,
    },
    {
      _id: "8",
      title: "Vero Moda",
      src: "images/Top/Women Black Solid Victorian Crop Fitted Top.png",
      description: "Women Black Solid Fitted Top",
      category: "top",
      price: 19,

      count: 1,
    },
    {
      _id: "9",
      title: "Vero Moda",
      src: "images/T Shirt/Women Lavender Printed Round Neck T-shirt.png",
      description: "Women Lavender Printed T-shirt",
      category: "tshirt",
      price: 50,
      count: 1,
    },
    {
      _id: "10",
      title: "Vero Moda",
      src:
        "images/Top/Women Mustard Yellow Solid Twisted Cropped Fitted Top.png",
      description: "Women Mustard Yellow Fitted Top",
      category: "top",
      price: 15,
      count: 1,
    },
    {
      _id: "11",
      title: "Vero Moda",
      src: "images/T Shirt/Women Pink Printed Round Neck T-shirt.png",
      description: "Women Pink Printed Round Neck T-shirt",
      category: "tshirt",
      price: 10,
      count: 1,
    },
    {
      _id: "12",
      title: "Vero Moda",
      src: "images/Top/Women White Solid Blouson Top.png",
      description: "Women White Solid Blouson Top",
      category: "top",
      price: 17,
      count: 1,
    },
  ]);
  const [cart, setCart] = useState([]);
  const [total, setTotal] = useState(0);

  const addCart = (id) => {
    var products = [...dup];
    const check = cart.every((item) => {
      return item._id !== id;
    });
      
    if (check) {
      const data = products.filter((product) => {
        return product._id === id;
      });
     
      setCart([...cart, ...data]);
      
    } else {
      alert("The product has been added to cart.");
    }
  };
  const removeQuantity = (id) => {
  
    cart.forEach((product) => {
      if (product._id === id) {
        product.count === 1 ? (product.count = 1) : (product.count -= 1);
      }
    });
    setCart(cart);
    totalAmount();
  };

  const addQuantity = (id) => {
   
    cart.forEach((product) => {
      if (product._id === id) {
        product.count += 1;
      }
    });
    setCart(cart);
    totalAmount();
  };

  const totalAmount = () => {
   
    const totalamt = cart.reduce((prev, product) => {
      return prev + product.price * product.count;
    }, 0);
    setTotal(totalamt);
  };
  const removeFromCart = id =>{
    if(window.confirm("Do you want to delete this product?")){
        
        cart.forEach((item, index) =>{
            if(item._id === id){
                cart.splice(index, 1)
            }
        })
        setCart(cart)
        totalAmount();
        
    }
   
};

  const mounted = useRef();
  useEffect(() => {
    if (!mounted.current) {
      // do componentDidMount logic
      const dataCart = JSON.parse(localStorage.getItem("dataCart"));
      if (dataCart !== null) {
        setCart(dataCart);
      }
      const dataTotal = JSON.parse(localStorage.getItem('dataTotal'));
      if (dataTotal !== null) {
        setTotal(dataTotal);
      }
      mounted.current = true;
    } else {
      // do componentDidUpdate logic
      localStorage.setItem("dataCart", JSON.stringify(cart));
      localStorage.setItem("dataTotal", JSON.stringify(total));
    }
  });

  return (
    <ProductContext.Provider value={[dup, cart, setCart, addCart, addQuantity, removeQuantity, total,totalAmount, removeFromCart]}>
      {props.children}
    </ProductContext.Provider>
  );
};
