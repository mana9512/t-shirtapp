import axios from "axios";
import {
  ITEM_ADDED,
  ADD_ITEM,
  REMOVE_ITEM,
  REMOVED_FROM_CART
} from "./types";

export const addToCart = (id) =>  {
  return {
      type: ITEM_ADDED,
      payload: id,
}
};
export const addItem = (id) =>  {
  return {
      type: ADD_ITEM,
      payload: id,
}
};
export const removeItem = (id) =>  {
  return {
      type: REMOVE_ITEM,
      payload: id,
}
};
export const removeFromCart = (id) =>  {
  return {
      type: REMOVED_FROM_CART,
      payload: id,
}
};